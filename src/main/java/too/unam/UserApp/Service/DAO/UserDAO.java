package too.unam.UserApp.Service.DAO;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import too.unam.UserApp.Interface.IUserDAO;
import too.unam.UserApp.Model.Role;
import too.unam.UserApp.Model.User;

import org.hibernate.query.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

@Repository
@Transactional
public class UserDAO implements IUserDAO {

    @Autowired
    private SessionFactory sessionFactory;

    public User getUser (int id){
        return sessionFactory.getCurrentSession().get(User.class, id);
    }

    public User getUserByEmail(String email){
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<User> criteria = builder.createQuery(User.class);
        Root<User> root = criteria.from(User.class);

        criteria.select(root).where(builder.equal(root.get("email"), email));
        Query<User> query = session.createQuery(criteria);
        User user = query.uniqueResult();
        return user;
    }

    public User getUser(String username){
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<User> criteria = builder.createQuery(User.class);
        Root<User> root = criteria.from(User.class);

        criteria.select(root).where(builder.equal(root.get("username"), username));
        Query<User> query = session.createQuery(criteria);
        User user = query.uniqueResult();
        return user;
    }

    public User getUser (String username, String password) {
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<User> criteria = builder.createQuery(User.class);
        Root<User> root = criteria.from(User.class);

        criteria.select(root).where(builder.and(
                builder.equal(root.get("username"), username),
                builder.equal(root.get("password"), password)));
        Query<User> query = session.createQuery(criteria);
        User user = query.getSingleResult();
        return user;
    }

    public Role getRole(String roleName){
        Session session = sessionFactory.getCurrentSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Role> criteria = builder.createQuery(Role.class);
        Root<Role> root = criteria.from(Role.class);

        criteria.select(root).where(builder.equal(root.get("roleName"), roleName));
        Query<Role> query = session.createQuery(criteria);
        Role role = query.getSingleResult();
        return role;
    }

    public void updateUser(User user){
        sessionFactory.getCurrentSession().update(user);
    }

    public void saveUser(User user){
        sessionFactory.getCurrentSession().save(user);
    }

    public void deleteUser(User user) {
        sessionFactory.getCurrentSession().delete(user);
    }
}
