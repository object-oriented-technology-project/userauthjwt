package too.unam.UserApp.Control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import too.unam.UserApp.Model.DTO.AuthenticationSignUp;
import too.unam.UserApp.Model.Role;
import too.unam.UserApp.Model.User;
import too.unam.UserApp.Service.DAO.UserDAO;

@Service
public class UserControl {

    @Autowired
    private UserDAO userDAO;

    /**
     * Metodo que se encarga de dar de alta un nuevo usuario.
     * @param authenticationSignUp
     */
    public void signUp(AuthenticationSignUp authenticationSignUp) {
        try {
            //Revisa si el Rol es correcto
            Role role = userDAO.getRole(authenticationSignUp.getRole());
            if (role == null) {
                throw new ResponseStatusException(HttpStatus.CONFLICT, "ROLE");
            }
            //Revisa si no hay otro usuario con el mismo userName
            User nullUser = userDAO.getUser(authenticationSignUp.getUsername());
            if (nullUser != null) {
                throw new ResponseStatusException(HttpStatus.CONFLICT, "USERNAME");
            }

            //Revisa si no hay otro usuario con el mismo email
            User nullUserEmail = userDAO.getUserByEmail(authenticationSignUp.getEmail());
            if (nullUserEmail != null) {
                throw new ResponseStatusException(HttpStatus.CONFLICT, "EMAIL");
            }
            User user = new User(
              authenticationSignUp.getUsername(),
              authenticationSignUp.getName(),
              authenticationSignUp.getEmail(),
              authenticationSignUp.getPassword(),
              role
            );

            userDAO.saveUser(user);

        } catch (ResponseStatusException err){
            throw err;
        } catch (Exception ex) {
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UNIDENTIFIED ERROR: " +ex);
        }
    }

    public User getUser(String username){
        return userDAO.getUser(username);
    }

    public void changeUserData(AuthenticationSignUp authenticationSignUp, User user){
        if(!authenticationSignUp.getUsername().equals(user.getUsername())){
            throw new ResponseStatusException(HttpStatus.CONFLICT, "USERNAME");
        }

        //Revisa si no hay otro usuario con el mismo email
        User nullUserEmail = userDAO.getUserByEmail(authenticationSignUp.getEmail());
        if (nullUserEmail != null && user != nullUserEmail) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "EMAIL");
        }

        user.setEmail(authenticationSignUp.getEmail());
        user.setName(authenticationSignUp.getName());
        user.setPassword(authenticationSignUp.getPassword());
        userDAO.updateUser(user);
    }

    public void deleteUser(User user) {
        userDAO.deleteUser(user);
    }
}
