package too.unam.UserApp.API;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import too.unam.UserApp.Control.UserControl;
import too.unam.UserApp.Model.DTO.AuthenticationRequest;
import too.unam.UserApp.Model.DTO.AuthenticationResponse;
import too.unam.UserApp.Model.DTO.AuthenticationSignUp;
import too.unam.UserApp.Model.User;
import too.unam.UserApp.Service.MyUserDetailsService;
import too.unam.UserApp.Util.JwtUtil;
import org.springframework.security.core.Authentication;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

@RestController
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class UserAPI {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private MyUserDetailsService myUserDetailsService;

    @Autowired
    private JwtUtil jwtTokenUtil;

    @Autowired
    private UserControl userControl;


    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<?> createAutheticationToken(@RequestBody AuthenticationRequest authenticationRequest, HttpServletResponse response) throws Exception{
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword()));
        } catch(BadCredentialsException e){
            throw new Exception("Incorrect username or password", e);
        }

        return responseWithCookie(authenticationRequest, response);
    }

    @RequestMapping(value="/signup", method = RequestMethod.POST)
    public ResponseEntity<?> signUp(@RequestBody AuthenticationSignUp authenticationSignUp, HttpServletResponse response) throws Exception{
        try {
            userControl.signUp(authenticationSignUp);
            AuthenticationRequest authenticationRequest = new AuthenticationRequest(authenticationSignUp.getUsername(), authenticationSignUp.getPassword());
            return responseWithCookie(authenticationRequest, response);
        } catch(ResponseStatusException responseStatusException){
            if(responseStatusException.getReason().equals("ROLE")){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("ROLE");
            } else if (responseStatusException.getReason().equals("USERNAME")){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("USERNAME");
            } else if (responseStatusException.getReason().equals("EMAIL")){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("EMAIL");
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("UNDEFINED ERROR");
            }
        }
    }

    @RequestMapping(value = "/autoLogin", method = RequestMethod.POST)
    public ResponseEntity<?> tryLogin(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        User user = userControl.getUser(userDetails.getUsername());
        return ResponseEntity.ok(new AuthenticationResponse(user.getName(), user.getRole().getRoleName(), user.getUsername()));
    }

    @RequestMapping(value = "/getUserData", method = RequestMethod.POST)
    public ResponseEntity<?> getUserData(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        User user = userControl.getUser(userDetails.getUsername());
        return ResponseEntity.ok(new AuthenticationResponse(user.getName(), user.getRole().getRoleName(), user.getUsername(), user.getEmail(), user.getPassword()));
    }

    @RequestMapping(value = "/changeUserData", method = RequestMethod.PUT)
    public ResponseEntity<?> changeUserData(@RequestBody AuthenticationSignUp authenticationSignUp){
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            final UserDetails userDetails = (UserDetails) authentication.getPrincipal();
            User user = userControl.getUser(userDetails.getUsername());
            userControl.changeUserData(authenticationSignUp, user);
            return ResponseEntity.ok("DATA CHANGED");
        }catch(ResponseStatusException responseStatusException){
            if (responseStatusException.getReason().equals("USERNAME")){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("USERNAME");
            } else if (responseStatusException.getReason().equals("EMAIL")){
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("EMAIL");
            } else {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("UNDEFINED ERROR");
            }
        }
    }

    @RequestMapping(value = "/deleteUser", method = RequestMethod.DELETE)
    public void deleteUser(HttpServletRequest httpServletRequest, HttpServletResponse response){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        User user = userControl.getUser(userDetails.getUsername());
        userControl.deleteUser(user);
        logout(httpServletRequest, response);
    }

    @RequestMapping(value = "/logOut", method = RequestMethod.POST)
    public void logout(HttpServletRequest httpServletRequest, HttpServletResponse response){
        Cookie[] cookies = httpServletRequest.getCookies();
        for(Cookie cookie: cookies){
            cookie.setMaxAge(0);
            cookie.setValue(null);
            response.addCookie(cookie);
        }
    }

    private ResponseEntity<?> responseWithCookie(AuthenticationRequest authenticationRequest, HttpServletResponse response){
        final User user = userControl.getUser(authenticationRequest.getUsername());
        final String jwt = jwtTokenUtil.generateToken(user);

        Cookie cookie = new Cookie(JwtUtil.AUTHORIZATION, jwt);
        cookie.setHttpOnly(true);
        response.addCookie(cookie);
        return ResponseEntity.ok(new AuthenticationResponse(user.getName(), user.getRole().getRoleName(), user.getUsername()));
    }
}
