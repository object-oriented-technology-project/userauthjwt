package too.unam.UserApp.Model.DTO;

public class AuthenticationResponse {
    private final String role;
    private final String name;
    private final String username;
    private String email;
    private String password;

    public AuthenticationResponse(String name, String role, String username) {
        this.name=name;
        this.role=role;
        this.username=username;
    }

    public AuthenticationResponse(String name, String role, String username, String email, String password) {
        this.role = role;
        this.name = name;
        this.username = username;
        this.email = email;
        this.password=password;
    }

    public String getRole() {
        return role;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword(){return password;}
}
